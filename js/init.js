var visibleForm = false;
var wow = new WOW(
    {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
    }
);
wow.init();

jQuery(document).ready( function () {
    "use strict";
    jQuery('.preloader').css('opacity', 0);
    setTimeout(function () {
        jQuery('.preloader').css('display', 'none');
    }, 3000);
});


jQuery('#contact-form').on('click touchstart', function() {
    "use strict";
    if (visibleForm == false) {
        jQuery('.hidden-section').removeClass('shown');
        jQuery('.hidden-section').addClass('hidden');
        jQuery('.contact-container').removeClass('contact-hidden');
        jQuery('.contact-container').addClass('contact-shown');
        jQuery('#contact-form').removeClass('fa-envelope-o');
        jQuery('#contact-form').addClass('fa-close');
        visibleForm = true;
    } else {
        jQuery('.contact-container').addClass('contact-hidden');
        jQuery('.contact-container').removeClass('contact-shown');
        setTimeout(function () {
            jQuery('.hidden-section').addClass('shown');
            jQuery('.hidden-section').removeClass('hidden');
            jQuery('#contact-form').addClass('fa-envelope-o');
            jQuery('#contact-form').removeClass('fa-close');
        }, 500);
        visibleForm = false;
    }

});
