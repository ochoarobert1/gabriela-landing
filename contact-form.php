<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once 'phpmailer/PHPMailerAutoload.php';

if (isset($_POST['inputName']) && isset($_POST['inputEmail'])&& isset($_POST['inputMessage'])) {

    //check if any of the inputs are empty
    if (empty($_POST['inputName']) || empty($_POST['inputEmail']) || empty($_POST['inputCity']) || empty($_POST['inputPhone']) ||empty($_POST['inputMessage'])) {
        $data = array('success' => false, 'message' => 'Porfavor complete todos los campos.');
        echo json_encode($data);
        exit;
    }

    //create an instance of PHPMailer
    $mail = new PHPMailer();

    $mail->From = $_POST['inputEmail'];
    $mail->FromName = $_POST['inputName'];
    $mail->AddAddress('hola@wegoapp.co'); //recipient 
    $mail->Subject = 'Nueva solicitud para repartidor!';
    $mail->Body = "Nombre: " . $_POST['inputName'] . "Tel.: " . $_POST['inputPhone'] . "Ciudad.: " . $_POST['inputCity'] . "\r\n\r\nMessage: " . stripslashes($_POST['inputMessage']);

    if (isset($_POST['ref'])) {
        $mail->Body .= "\r\n\r\nRef: " . $_POST['ref'];
    }

    if(!$mail->send()) {
        $data = array('success' => false, 'message' => 'El mensaje no se envió. Error: ' . $mail->ErrorInfo);
        echo json_encode($data);
        exit;
    }

    $data = array('success' => true, 'message' => '¡Gracias! Hemos recibido tu solicitud.');
    echo json_encode($data);

} else {

    $data = array('success' => false, 'message' => 'Por favor Llene todo el formulario.');
    echo json_encode($data);

}